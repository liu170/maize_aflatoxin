## The function calculated the alfltoxin risk index based on the mechnisam model ( Battilani et al 2013)
## THe input of the funtion include:
##            grid_year: the grid and year of the harvested samples:
##                the first column is year
##                the second column is longitude  
##                the third column is latitude 
##            weather: meteorological data, including GRID_NO LATITUDE LONGITUDE ALTITUDE DAY TEMPERATURE_MAX TEMPERATURE_MIN 
##                TEMPERATURE_AVG WINDSPEED VAPOURPRESSURE PRECIPITATION E0 yday year
## By Ningjing Liu, 2020-04-17, WFSR

generate_ARI <- function(grid_year, weather,nf){


  # Test data###
  ##save(weather,grid_year,file='D:/WURgit/MyToolBox/Maize/Model Aflatoxins Maize/serbia maize/data/gernerate_ARI_test.RData')
  # load('D:/WURgit/MyToolBox/Maize/Model Aflatoxins Maize/serbia maize/data/gernerate_ARI_test.RData')
  # nf=10
  # # ###
  #
  library("dplyr")
  library(data.table)
  library(rgeos)
  library(sp)
  library(tidyr)
  library("lubridate")
  
  
  ################## Selection of weather data ##########################
  ### find weather grid cell that is closest to wheat observation grid cell
  ##define unique grids from weather
  grids <- unique(cbind(weather$GRID_NO,weather$LONGITUDE,weather$LATITUDE))
  gridsspdata <- SpatialPointsDataFrame(coords=grids[,2:3],data=as.data.frame(grids[,1]))
  
  ##latlong combinations from myselection
  grid_year <- as.data.frame(sapply(grid_year, function(x) as.numeric(as.character(x))))
  longlat<-grid_year[,2:3]
  longlatsp<-SpatialPoints(longlat)
  ##find nearest grid cell using gDistance function
  longlatsp$nearest<-apply(gDistance(gridsspdata,longlatsp, byid=TRUE), 1, which.min)
  
  ##check grid number
  grid_year$GRID_NO <-NA
  grid_year$long_w <- NA
  grid_year$lat_w <- NA
  
  for(i in 1:nrow(grid_year)){
    index<-longlatsp$nearest[i]
    grid_year$GRID_NO[i]<-grids[index,1]
    grid_year$long_w[i]<-grids[index,2]
    grid_year$lat_w[i]<-grids[index,3]
  }
  
  ##convert weather dates to year and day of the year
  
  weather$year<-NA
  weather$yday<-NA
  weather$DAY<-as.character(weather$DAY)
  x<-as.Date(weather$DAY, "%Y%m%d")
  weather$year[]<-year(x)
  weather$yday[]<-yday(x)
  rm(x)
  
  weather<-arrange(weather,weather$year,weather$GRID_NO,weather$yday)
  grid <- unique(grid_year$GRID_NO)
  
  yearlist=c(min(grid_year[,1]):max(grid_year[,1]))
  ARI<-matrix(0,length(grid)*length(yearlist),nf)
  
  ##calculate ARI for each grid per year
  for (i in 1:length(yearlist)){
    ##per year
    for(ii in 1:length(grid)){
      ## per grid number
      grid_new <- grid[ii]
      weather_grid <- weather[weather$GRID_NO == grid_new,]
      weather_year <- weather_grid[which(weather_grid$year==yearlist[i]),]
      Temp_max_total <- weather_year[,6]
      Temp_min_total <- weather_year[,7]
      Temp_mean_total <- weather_year[,8]
      Rain_total <- weather_year[,11]
      VP=weather_year[,10]*0.1 # clear Weather
      
      # calculation of saturation vapour pressure
      es_tmin = 0.6108 * exp((17.27 * Temp_min_total)/(Temp_min_total+237.3))
      es_tmax = 0.6108 * exp((17.27 * Temp_max_total)/(Temp_max_total+237.3))
      es= (es_tmax + es_tmin)/2 # kPa
      RH_total = (VP/es)*100
      
      ##calculate growth period
      GDD = rep(0,length(Temp_min_total))
      GDD = as.matrix(GDD)
      for(iii in 1:length(Temp_min_total)){
        Tmin = Temp_min_total[iii]
        Tmax = Temp_max_total[iii] 
        Tbase = 6 
        Tcut = 30 # degC
        if(Tmin<Tbase){
          Tmin=Tbase
        }
        if(Tmax< Tbase){
          Tmax = Tbase
        }
        if(Tmin > Tcut){
          Tmin =Tcut
        }
        if (Tmax > Tcut){
          Tmax =Tcut
        }
        
        
        GDD[iii] = ((Tmin + Tmax)/2) - Tbase
      }
      
      GDD[1] = GDD[1] + 50 ## GDD @ emergence = 50
      GDD = cumsum(GDD)
      #          I = find(GDD < 751); 
      temp <- seq(from = 750, to = 1500, length=nf+1)
      for (j in 1:(length(temp)-1)){
        II = as.numeric(which(GDD > temp[j] & GDD < temp[j+1]))
        ## Select weather from crop flowering to harvest
        Rain = Rain_total[II]
        RH = RH_total[II]
        Temp_mean = Temp_mean_total[II]
        
        # DISPERSAL
        # Spore dispersal/fungal invasion is modelled according to Battilani et al 2013
        DIS_rain = rep(0,length(Rain)) 
        DIS_RH = rep(0,length(RH))
        DIS = rep(0,length(RH))
        
        idx<-which(Rain==0)
        DIS_rain[idx]<-1    
        idx2<-which(Rain>0)
        DIS_rain[idx2]<-0
        DIS <- DIS_rain
        
        DIS_RH[which(RH<80)] <-1
        DIS_RH[which(RH>=80)] <-0
        DIS<-cbind(DIS,DIS_RH)
        DIS<-cbind(DIS,rowSums(DIS))
        DIS<-DIS[,3]
        DIS[DIS==1]<- 0
        DIS[DIS==2]<- 1
        
        ## afla production according to Battilani (2013)
        # AFLA.T
        A= 4.84
        B=1.32
        C=5.59
        T_max = 47
        T_min=10
        
        AFLA = as.matrix(rep(0,length(Temp_mean)))
        
        for (jjj in 1:length(Temp_mean)){
          Temp = Temp_mean[jjj]
          Teq = (Temp - T_min)/(T_max-T_min)
          AFLA[jjj]<- (A *(Teq^B)*(1-Teq))^C
          AFLA[is.nan(AFLA)]<-0
        }
        ## GROWTH according to Battilani (2013)
        # GROWTH.T
        A= 5.98
        B=1.70
        C=1.43
        T_max = 48
        T_min=5
        
        GROWTH = as.matrix(rep(0,length(Temp_mean)))  
        
        for(kkk in 1:length(Temp_mean)){
          Temp <- Temp_mean[kkk]
          
          Teq = (Temp -T_min)/(T_max-T_min)
          GROWTH[kkk]<-(A*(Teq^B)*(1-Teq))^C
          GROWTH[is.nan(GROWTH)]<- 0
        }
        
        
        ## Calculate ARI
        Aflo_risk = AFLA * GROWTH *DIS;  #* DIS(DIS is considered as non limiting factor);
        Aflo_risk
        Aflo_risk_sum = sum(Aflo_risk)
        ARI[(i-1)*length(grid)+ii,j]<-Aflo_risk_sum
      }
      
    }
  }
  
  index_grid <- rep(grid,length(yearlist))
  index_year <- rep(yearlist,each=length(grid))
  colnames(ARI) <- paste0(c('ARI_'),c(1:nf))
  ARI <- cbind.data.frame(index_year,index_grid,ARI)
  
  output <- merge(grid_year,ARI,lookup,by.x=c("yearcode","GRID_NO"),by.y=c("index_year","index_grid"))
  
  return(output)
  
}
