# Maize Aflatoxin

### Model description:
The model was developed based on the combination of Bayesian network and a weather-driven mechanistic model. A total of 869 maize samples were collected from fields in Serbia between 2012 and 2018. The samples were analysed by LC-MS/MS to obtain the total aflatoxins levels. According to the EU regulations, the samples classified into high class (above 10ug/kg) and low class (below 10ug/kg). The entire dataset was split into two independent datasets by harvest year as follows: (1) training set including 752 observations collected from 2012-2016; (2) validating set including 117 observations collected from 2017-2018. Meteorological data of related fields were obtained from a public database, JRC Interpolated Meteorological Dataset.

### Mechanistic model
In order to allow the model run independently of external information, the flowering date and the harvest date were determined by temperature sum (750 and 1500 degree-days, respectively). The period from flowering date to harvest date was split into 8 sub-period (P1 to P8) equally. In each sub-period, the total aflatoxin risk index (ARI) was calculated according to a mechanistic model (Battilani, Leggieri, Rossi, et al., 2013) of aflatoxin B1 (AFB1), which is the dominant aflatoxin in maize. According to Battilani, et al. (2013), the production of AFB1 was consist of three parts, dispersal rate (DISP), Germination rate (GERM) and Fungal growth (GROWTH): 
DISP ={█(1,&R=0 and RH<80%@0,&otherwise)┤	(1)
GERM = 〖(4.84 * (1-((T-10)/37)) * 〖((T-10)/37)〗^1.32)〗^5.5	(2)
GROWTH = 〖(5.98 * (1-((T-5)/43)) * 〖((T-5)/43)〗^1.7)〗^1.43	(3)
 ARI =DISP*GERM*GROWTH	(4)

Where R is rain, RH relative humidity and T is temperature.
The modelling steps are detailed in the original papers by Battilani, et al. (2013).

### Implementation
The model was developed with R 3.6.1
Packages: bnlearn, gRain, dplyr, Rgraphviz, arules, stringr.
After installing the above packages, execute the file ‘afla_bn.R’
### Model input and output
Input: meteorological data of the field, including daliy; coordinates of the fields
Output: aflatoxins contamination levels per field.
### Uncertainty analysis
The model was developed by the data collected in Serbia. There performance may be differed if the model is applied in forecasting the contamination levels in other countries.
### Sensitivity analysis
The model was internal and external validated. In the training set, 390 of 462 low class records were correctly predicted as low class (specificity = 84%); 116 of 139 high class records were correctly predicted as high class (sensitivity = 84%); in total, 506 of 601 records were correctly classified (accuracy = 84%). In the internal validation set, 104of 124 low class records were correctly predicted as low class (sensitivity = 84%); 22 of 27 high class records were correctly predicted as high class (specificity = 82%); in total, 126 of 151 records were correctly classified (accuracy = 83%). In the external validation set, 65 of 97 low class records were correctly classified (sensitivity = 67%); 17 of 20 high class records were correctly classified (specificity = 85%); in total, 82 of 117 records were correctly classified (accuracy = 70%).
### Dependency
The meteorological data were obtained from JRC (https://agri4cast.jrc.ec.europa.eu/DataPortal/Index.aspx).
### Reference
Battilani, P., Leggieri, M. C., Rossi, V., & Giorni, P. (2013). AFLA-maize, a mechanistic model for Aspergillus flavus infection and aflatoxin B1 contamination in maize. Computers and electronics in agriculture, 94, 38-46.

##